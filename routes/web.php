<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/admin', 'ArticleController@showArticleTable')->middleware('admin');
Route::get('/admin/article', 'ArticleController@showArticleTable')->middleware('admin');
Route::get('/admin/insert', 'ArticleController@showInsertArticleForm')->middleware('admin');
Route::get('/admin/update/{id}', 'ArticleController@showUpdateArticleForm')->middleware('admin');
Route::get('/admin/user', 'ProfileController@showUser')->middleware('admin');
Route::get('/detail/{id}', 'ArticleController@showDetail');
Route::post('/admin/post', 'ArticleController@addArticle')->middleware('admin');
Route::put('/admin/{id}', 'ArticleController@updateArticle')->middleware('admin');
Route::get('/member/delete/{id}', 'ProfileController@deleteProfile')->middleware('admin');
Route::get('/article/delete/{id}', 'ArticleController@deleteArticle')->middleware('admin');
Route::get('/home', 'ArticleController@index')->name('home');
Route::get('/profile/{id}', 'ProfileController@showProfile');
Route::get('/profile/history/{id}', 'ProfileController@showUserHistory');
Route::get('/formInput', 'MainController@showFormInput');
Route::post('/formInput', 'MainController@updateTinggiBeratBadan');
Route::get('/idealBadan', 'MainController@showKondisiBadan');
Route::get('/polaMakan', 'MainController@showPolaMakan');

Route::get('/login', 'LoginController@showLoginForm')->name('login')->middleware('guest');
Route::post('/login', 'LoginController@login')->middleware('guest');
Route::post('/logout', 'LoginController@logout')->middleware('auth')->name('logout');
Route::get('/register', 'RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'RegisterController@register');

Route::get('/', 'ArticleController@index');
Route::get('/nutrisi', 'MainController@showKebutuhanGizi');
