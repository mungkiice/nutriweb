# Website NutriWeb

NutriWeb adalah aplikasi berbasis website yang berguna membantu pengguna untuk menentukan asupan nutrisi dengan jumlah yang tepat berdasarkan berat badan, tinggi badan, dan energi yang dibutuhkan pengguna sebagai atlit. 

Kebutuhan zat gizi merupakan salah satu faktor penting yang diperhatikan agar fungsi tubuh kita normal. Kebutuhan gizi yang dipenuhi dengan baik adalah seimbangnya zat gizi antara pemasukan dan pengeluaran tubuh. Kelebihan ataupun kekurangan pemasukan zat gizi berdampak buruk bagi tubuh. Tentunya untuk zat gizi yang tepat didapatkan dengan pola makan yang sesuai.

## Fungsi Aplikasi

#### Versi 1.0

1. Login member
2. Register member
3. Halaman profil member

#### Versi 2.0

1. Admin Panel
2. Fungsi CRUD artikel
3. Fungsi CRUD member

#### Versi 3.0

1. Perhitungan berat badan ideal
2. Perhitungan jumlah nutrisi
3. Penentuan pola makan

### Prasyarat

Untuk menjalankan website secara dikomputer anda, dibutuhkan :
1. [Composer](https://getcomposer.org/download/)
2. Apache2
3. MySql
4. PHP ^7.0
5. PHP-bcmath

### Instalasi

Step - step untuk menjalankan website di workspace secara lokal

Buka terminal/command prompt, kemudian clone projek ini

```
git clone https://gitlab.com/mungkiice/nutriweb.git
```

Pindah ke direktori projek

```
cd nutriweb
```

Install dependencies

```
composer install
```

Buat file .env dengan isi template seperti file .env.example, Kemudian isi informasi database

```
DB_DATABASE=(NAMA_DATABASE)
DB_USERNAME=(USERNAME_DATABASE)
DB_PASSWORD=(PASSWORD_DATABASE)
```

Generate Key

```
php artisan key:generate
```

Generate tabel beserta data bawaan

```
php artisan migrate --seed
```

Projek sudah siap untuk dijalankan

```
php artisan serve
```

Kemudian akses website menggunakan browser dengan URL

```
localhost:8000
```

<!-- ## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system -->

## Dikembangkan menggunakan library/template

* [Laravel 5.7](https://laravel.com/docs/5.7) - PHP Website Framework
* [Bootstrap 3.3.7](https://getbootstrap.com/docs/3.3/) - HTML, CSS, & JS Framework

<!-- ## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us. -->

## Versioning

Untuk melihat versi - versi yang tersedia, bisa dilihat di [tags on this repository](https://gitlab.com/mungkiice/nutriweb/tags).

## Pengembang

* **Dindy Fitriannora** - *Pengembang* - [Account Page](https://gitlab.com/iamdindy)
* **Muhammad Afif Fauzi** - *Pengembang* - [Account Page](https://gitlab.com/afiffauz1)
* **Muhammad Iqbal Kurniawan** - *Pengembang* - [Account Page](https://gitlab.com/mungkiice)
* **Muhammad Zaki Nabil** - *Pengembang* - [Account Page](https://gitlab.com/zakinabil)

Daftar [kontributor](https://gitlab.com/mungkiice/nutriweb/graphs/master) yang berpartisipasi dalam projek.

## Lisensi

Projek ini dibawah lisensi MIT - lihat di [LICENSE](LICENSE) file untuk lebih jelas

<!-- ## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc -->
